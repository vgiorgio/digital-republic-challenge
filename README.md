<div align="center">
    <h1> </h1>
    <img width="300" src="./frontend/src/assets/img/logo.png" />
    <hr />
    <p>
    <a href="https://nodejs.org/en/"><img src="https://img.shields.io/badge/backend-Node.js-green" alt="API main language" /></a>
    <a href="https://pt-br.reactjs.org/"><img src="https://img.shields.io/badge/frontend-React-blue" alt="API main language" /></a>
    <a href="./LICENSE"><img src="https://img.shields.io/badge/license-MIT-red" alt="Repository license" /></a>
    </p>
</div>

## 📜 Descrição

<p>
    <b>PingereON</b> é uma SPA (Single-page application) de consulta à quantidade de latas de tinta necessárias com base nas medidas passadas pelo usuário.
</p>
<p>
    O projeto foi desenvolvido como resposta ao desafio técnico proposto pela Digital Republic.
</p>

<br>

---

<br>

## 🔍 Visualização

<div align="center">

<img src="./frontend/src/assets/demos/demo-1.png" />

<br>

<img src="./frontend/src/assets/demos/demo-2.png" />

<br>

<img src="./frontend/src/assets/demos/demo-3.png" />

<br>

<img src="./frontend/src/assets/demos/demo-4.png" />

</div>

<br>

---

<br>

## 🛠️ Tecnologias e ferramentas

Foram utilizadas as seguintes tecnologias e ferramentas no projeto:

### Backend:

- [Javascript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [Node.js](https://nodejs.org/en/)
- [Express](https://expressjs.com/pt-br/)
- [Insomnia](https://insomnia.rest/)
- [Jest](https://jestjs.io/pt-BR/)

### Frontend:

- [Typescript](https://www.typescriptlang.org/)
- [React](https://pt-br.reactjs.org/)
- [Styled-components](https://styled-components.com/)
- [Material UI](https://mui.com/pt/)

### UI & UX:

- [Figma](https://www.figma.com/)

<br>

---

<br>

## 🖌️ Prototipagem

<br>

- <a href="https://www.figma.com/file/tHelL0SNIAFLhzZ8Vt3Dcw/Digital-Republic?node-id=0%3A1">Figma</a>

<br>

---

<br>

# 🌟 Rodando a aplicação

## Requerimentos

- [Git](https://git-scm.com/) instalado
- [Node](https://node.js.org/) instalado
- [Npm](https://www.npmjs.com/) instalado
- [VSCode](https://code.visualstudio.com/) instalado (mas você pode utilizar outra IDE, se preferir)

<br>

### Siga as etapas para a aplicação funcionar corretamente:

<br>

#### 🟠 No terminal bash (Git), clone este repositório

```
git clone https://gitlab.com/vgiorgio/digital-republic-challenge.git
```

<br>

#### 🟠 Acesse a pasta do projeto pelo terminal

```
cd digital-republic-challenge
```

<br>

#### 🟠 Abra o projeto no editor de texto (VSCode)

```
code .
```

<br>

### Backend:

#### 🟠 Abra o terminal e acesse a pasta `backend`

```
cd backend
```

<br>

#### 🟠 (Opcional) Acesse o arquivo `.env.example` e copie o seu conteúdo. Crie um novo arquivo chamado `.env` e cole o conteúdo nele.

```
# API

PORT=3001
```

#### <b>Atenção</b>:

- O campo PORT altera a porta utilizada para rodar a API. Se não for definida, a API rodará normalmente na porta 3001. Caso queira definir, copie o conteúdo do arquivo `.env.example`, cole-o em um novo arquivo `.env` na raíz do projeto e altere o campo PORT de acordo com sua preferência.

<br>

#### 🟠 Execute o comando para instalar as dependências do projeto no backend

```
npm install
```

<br>

#### 🟠 Rode a API

```
npm start
```

#### Você verá essa mensagem: `Api running on 3001`.

<br>

### Frontend:

#### 🟠 Deixe o terminal anterior aberto e rodando e abra um novo terminal. Acesse a pasta frontend nesse novo terminal

```
cd frontend
```

<br>

#### 🟠 Execute o comando para instalar as dependências do projeto no frontend

```
npm install
```

<br>

#### 🟠 Rode a aplicação

```
npm start
```

#### A aplicação irá rodar em `localhost:3000`

<br>

---

<br>

## 🔃 Rota da API

#### 🪧 `/cans`

Rota para <b>encontrar</b> a quantidade de latas necessárias de acordo com as medidas das quatro paredes informadas.<br>
Método: `POST`<br>

Template para enviar os dados:

```
{
	"queries": [
		{
			"width": 1.5,
			"height": 1.5,
			"windowsNum": 0,
			"doorsNum": 0
		},
		{
			"width": 5,
			"height": 3,
			"windowsNum": 0,
			"doorsNum": 0
		},
		{
			"width": 5,
			"height": 3,
			"windowsNum": 0,
			"doorsNum": 0
		},
		{
			"width": 5,
			"height": 3,
			"windowsNum": 0,
			"doorsNum": 0
		}
	]
}
```

<br>
Resposta:

```
{
	"sm": 5,
	"md": 0,
	"lg": 2,
	"xlg": 0
}
```

<br>

---

<br>

<div align="center">
    <sub>Copyright © 2022</sub>
    <p>MIT licensed</p>
    <h3>✨ Desenvolvido por Vitor Giorgio ✨</h3>
</div>
