const queryRouter = require("./queryRouter");

module.exports = (app) => {
  app.use(queryRouter);
};
