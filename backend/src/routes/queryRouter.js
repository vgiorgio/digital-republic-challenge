const { Router } = require("express");
const { getCans } = require("../controllers/queryController");

const validateQuery = require("../services/validations/validations");

const queryRouter = Router();

queryRouter.post("/cans", validateQuery(), getCans);

module.exports = queryRouter;
