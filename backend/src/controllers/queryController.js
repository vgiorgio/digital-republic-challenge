const { validationResult } = require("express-validator");
const findLitersAmount = require("../services/queryCalculations/queryCalculations");

const getCans = (req, res) => {
  const wallsData = req.body.queries;

  /* treat automatic string casting */
  const obj = wallsData.map((value) => {
    return {
      width: parseFloat(value.width),
      height: parseFloat(value.height),
      windowsNum: parseFloat(value.windowsNum),
      doorsNum: parseFloat(value.doorsNum),
    };
  });

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const cans = findLitersAmount(obj);

    if (cans.error) {
      return res.status(400).json(cans.message);
    }

    return res.status(200).json(cans);
  } catch (e) {
    return res.status(500).json(e);
  }
};

module.exports = { getCans };
