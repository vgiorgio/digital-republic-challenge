/* CONSTANT VALUES */
const DOOR_HEIGHT = 1.9;
const DOOR_WIDTH = 0.8;

const WINDOW_HEIGHT = 1.2;
const WINDOW_WIDTH = 2;

const SMALL_CAN = 0.5;
const MEDIUM_CAN = 2.5;
const LARGE_CAN = 3.6;
const EXTRA_LARGE_CAN = 18;

/**
 * Check if the windows' and doors' total area is less than 50% of the wall's area
 * @param {*} windowsNum
 * @param {*} doorsNum
 * @param {*} wallHeight
 * @param {*} wallWidth
 */
const checkWindowsAndDoors = (windowsNum, doorsNum, wallHeight, wallWidth) => {
  const wallArea = wallHeight * wallWidth;

  const windowsArea = windowsNum * (WINDOW_HEIGHT * WINDOW_WIDTH);
  const doorsArea = doorsNum * (DOOR_HEIGHT * DOOR_WIDTH);

  /* if there are no windows or doors, it will be 0 */
  if (windowsArea + doorsArea > wallArea * 0.5) {
    return {
      error: true,
      message:
        "O total de área das janelas e portas deve ser no máximo 50% da área da parede",
    };
  }

  return false;
};

/**
 * Check whether the wall is at least 30 cm higher than the door
 * @param {*} wallHeight
 */
const checkDoorHeight = (wallHeight) => {
  if (wallHeight < 0.3 + DOOR_HEIGHT) {
    return {
      error: true,
      message:
        "A altura da parede com porta deve ser no mínimo 30 cm a mais que a altura da porta",
    };
  }

  return false;
};

/**
 * Find how many cans of each kind are necessary according to the amount of liters
 * @param {*} liters
 * @returns object containing the kinds of cans and their quantities
 */
const checkCans = (liters) => {
  let cans = {
    sm: 0,
    md: 0,
    lg: 0,
    xlg: 0,
  };

  let missingLiters = liters;

  while (missingLiters) {
    if (missingLiters >= EXTRA_LARGE_CAN) {
      cans.xlg++;
      missingLiters -= EXTRA_LARGE_CAN;
    } else if (missingLiters >= LARGE_CAN) {
      cans.lg++;
      missingLiters -= LARGE_CAN;
    } else if (missingLiters >= MEDIUM_CAN) {
      cans.md++;
      missingLiters -= MEDIUM_CAN;
    } else if (missingLiters >= SMALL_CAN) {
      cans.sm++;
      missingLiters -= SMALL_CAN;
    } else if (missingLiters < SMALL_CAN) {
      cans.sm++;
      return cans;
    }
  }
};

/**
 * Find the amount of paint liters necessary and pass it to the `checkCans` function
 * @param {*} walls
 * @returns an object containing the cans
 */
const findLitersAmount = (walls) => {
  let necessaryLiters = 0;

  for (let i = 0; i < 4; i++) {
    if (walls[i].doorsNum) {
      let errors = checkDoorHeight(walls[i].height);
      if (errors.error) {
        return errors;
      }
    }

    let errors = checkWindowsAndDoors(
      walls[i].windowsNum,
      walls[i].doorsNum,
      walls[i].height,
      walls[i].width
    );

    if (errors.error) {
      return errors;
    }
  }

  walls.map((wall) => {
    let unusedArea =
      wall.doorsNum * DOOR_HEIGHT * DOOR_WIDTH +
      wall.windowsNum * WINDOW_HEIGHT * WINDOW_WIDTH;

    necessaryLiters += (wall.width * wall.height - unusedArea) / 5;
  });

  const cans = checkCans(necessaryLiters);

  return cans;
};

/* const walls = [
  {
    width: 5,
    height: 3,
    windowsNum: 2,
    doorsNum: 1,
  },
  {
    width: 5,
    height: 3,
    windowsNum: 0,
    doorsNum: 0,
  },
  {
    width: 5,
    height: 3,
    windowsNum: 0,
    doorsNum: 0,
  },
  {
    width: 5,
    height: 3,
    windowsNum: 0,
    doorsNum: 0,
  },
];

console.log(findLitersAmount(walls)); */

module.exports = findLitersAmount;
