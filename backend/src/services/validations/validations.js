const { body } = require("express-validator");

const validateQuery = () => {
  return [
    body("queries.*.width").exists().withMessage("Campo width é obrigatório"),
    body("queries.*.height").exists().withMessage("Campo height é obrigatório"),
    body("queries.*.windowsNum")
      .exists()
      .withMessage("Campo windowsNum é obrigatório"),
    body("queries.*.doorsNum")
      .exists()
      .withMessage("Campo doorsNum é obrigatório"),
  ];
};

module.exports = validateQuery;
