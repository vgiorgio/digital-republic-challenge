const request = require("supertest");
const app = require("../src/index");

describe("Testar funcionalidades de API", () => {
  it("Deve conseguir retornar a quantidade de latas corretamente", async () => {
    const queries = {
      queries: [
        {
          width: 5,
          height: 3,
          windowsNum: 2,
          doorsNum: 1,
        },
        {
          width: 5,
          height: 3,
          windowsNum: 0,
          doorsNum: 0,
        },
        {
          width: 5,
          height: 3,
          windowsNum: 0,
          doorsNum: 0,
        },
        {
          width: 5,
          height: 3,
          windowsNum: 0,
          doorsNum: 0,
        },
      ],
    };

    const response = await request(app).post("/cans").send(queries);

    expect(response.status).toBe(200);

    expect(response.body.sm).toBe(3);
    expect(response.body.md).toBe(1);
    expect(response.body.lg).toBe(2);
    expect(response.body.xlg).toBe(0);
  });

  it("Deve retornar um erro caso algum dos atributos estejam faltando", async () => {
    const queries = {
      queries: [
        {
          width: 5,
          height: 3,
          windowsNum: 0,
        },
      ],
    };

    const response = await request(app).post("/cans").send(queries);

    expect(response.status).toBe(400);
    expect(response.body.errors[0].msg).toBe("Campo doorsNum é obrigatório");
  });

  it("Deve retornar um erro caso algum dos atributos esteja no formato errado", async () => {
    const queries = {
      queries: [
        {
          width: 5,
          height: 3,
          windowsNum: 0,
          doorsNum: "teste",
        },
      ],
    };

    const response = await request(app).post("/cans").send(queries);

    expect(response.status).toBe(400);
    expect(response.body.errors[0].msg).toBe("Deve ser um número");
  });
});
