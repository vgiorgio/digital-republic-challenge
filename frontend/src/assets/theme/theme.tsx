/* Design Colors */
export const colors = {
  light: "#F0F0F0",
  darkOrange: "rgba(215, 116, 61, 1)",
  lightOrange: "rgba(215, 116, 61, 0.69)",
};
