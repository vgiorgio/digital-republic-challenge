import styled from "styled-components";

export const NavSection = styled.nav`
  margin: 0;
  padding: 0;
  width: 100%;
  height: 150px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const NavLogo = styled.img`
  width: 350px;
  height: 160px;
`;
