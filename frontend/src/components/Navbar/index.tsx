import React from "react";
import { colors } from "../../assets/theme/theme";
import { NavSection, NavLogo } from "./styles";

function Navbar() {
  return (
    <NavSection style={{ backgroundColor: colors.lightOrange }}>
      <NavLogo src={require("../../assets/img/logo.png")} alt={"Logo"} />
    </NavSection>
  );
}

export default Navbar;
