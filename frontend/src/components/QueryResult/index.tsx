import React, { useEffect, useState } from "react";
import { colors } from "../../assets/theme/theme";
import {
  ImageSection,
  MainSection,
  ResultNumber,
  ResultParagraph,
  ResultParagraphSection,
  ResultSection,
  ResultTitle,
  Styles,
} from "./styles";
import { ReactComponent as ResultImage } from "../../assets/img/result-image.svg";
import { ReactComponent as PaintIcon } from "../../assets/img/paint-icon.svg";

function QueryResult({ cans }: any) {
  const [result, setResult] = useState(false);

  useEffect(() => {
    // console.log(cans);
    if (cans.sm || cans.md || cans.lg || cans.xlg) {
      setResult(true);
    } else {
      setResult(false);
    }
  }, [cans]);

  return (
    <>
      {result && (
        <MainSection style={{ color: colors.light }}>
          <ImageSection>
            <ResultImage />
          </ImageSection>
          <ResultSection style={{ backgroundColor: colors.darkOrange }}>
            <ResultTitle>Latas de tinta necessárias</ResultTitle>
            <ResultParagraphSection>
              {cans.sm ? (
                <ResultParagraph>
                  <PaintIcon style={Styles.icon} />
                  <ResultNumber>{cans.sm}</ResultNumber> Latas de <b>0,5 L</b>
                </ResultParagraph>
              ) : null}
              {cans.md ? (
                <ResultParagraph>
                  <PaintIcon style={Styles.icon} />
                  <ResultNumber>{cans.md}</ResultNumber> Latas de <b>2,5 L</b>
                </ResultParagraph>
              ) : null}
              {cans.lg ? (
                <ResultParagraph>
                  <PaintIcon style={Styles.icon} />
                  <ResultNumber>{cans.lg}</ResultNumber> Latas de <b>3,6 L</b>
                </ResultParagraph>
              ) : null}
              {cans.xlg ? (
                <ResultParagraph>
                  <PaintIcon style={Styles.icon} />
                  <ResultNumber>{cans.xlg}</ResultNumber> Latas de <b>18 L</b>
                </ResultParagraph>
              ) : null}
            </ResultParagraphSection>
          </ResultSection>
        </MainSection>
      )}
    </>
  );
}

export default QueryResult;
