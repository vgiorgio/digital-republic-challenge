import styled from "styled-components";

export const MainSection = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 5%;
  text-align: justify;
  flex-wrap: wrap;
`;

export const ImageSection = styled.div`
  -webkit-animation: move 1.5s infinite alternate;
  animation: move 1.5s infinite alternate;

  @-webkit-keyframes move {
    0% {
      transform: translateY(0);
    }
    100% {
      transform: translateY(-30px);
    }
  }

  @keyframes move {
    0% {
      transform: translateY(0);
    }
    100% {
      transform: translateY(-30px);
    }
  }
`;

export const ResultSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border-radius: 10px;
  width: 30%;
  margin-left: 5%;
  height: 500px;
  box-shadow: 0 0 1rem rgba(215, 116, 61, 0.69);

  @media (max-width: 1200px) {
    margin-top: 15px;
    text-align: center;
    width: 100%;
    padding: 10px;
    margin-left: 0;
  }
`;

export const ResultTitle = styled.h1`
  font-size: 30px;
  margin-top: 12.5%;
`;

export const ResultParagraphSection = styled.div`
  display: flex;
  font-size: 24px;
  flex-direction: column;
  margin-top: 75px;
  align-items: center;
  justify-content: center;
`;

export const ResultParagraph = styled.p`
  padding: 15px 0;
`;

export const ResultNumber = styled.span`
  font-weight: bold;
  font-size: 30px;
  box-shadow: 0 0 0.2rem #f0f0f0;
  padding: 10px 15px;
  margin-right: 10px;
  border-radius: 10px;

  &:hover {
    background-color: #f0f0f0;
    transition-duration: 0.3s;
    color: rgba(215, 116, 61, 0.69);
    cursor: pointer;
  }

  &:not(:hover) {
    transition-duration: 0.3s;
  }
`;

export const Styles = {
  icon: {
    width: 30,
    height: 30,
    marginRight: 15,
  },
};
