import React from "react";
import { colors } from "../../assets/theme/theme";
import { MainSection, SectionTitle, SectionText } from "./styles";

function TextSection() {
  return (
    <MainSection style={{ color: colors.darkOrange }}>
      <SectionTitle>Nosso serviço</SectionTitle>
      <SectionText>
        Com a nossa aplicação, você pode verificar quantas latas de tinta serão
        necessárias para determinada aplicação
        <br />
        <br />
        Basta preencher o seguinte formulário e consultaremos para você!
      </SectionText>
    </MainSection>
  );
}

export default TextSection;
