import styled from "styled-components";
import { colors } from "../../assets/theme/theme";

export const MainSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 5%;
  text-align: justify;
`;

export const MainForm = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 5%;
  text-align: justify;
`;

export const InputContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const InputSection = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid rgba(215, 116, 61, 0.69);
  border-radius: 15px;
  padding: 20px;
  margin: 0 15px;
`;

export const InputArea = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

export const TextSectionDivider = styled.p`
  font-size: 26px;
  font-weight: bold;
  align-self: center;
  margin-bottom: 18px;
`;

export const SectionTitle = styled.h1`
  font-size: 48px;

  @media (max-width: 850px) {
    font-size: 32px;
    text-align: center;
  }
`;

export const SectionSubtitle = styled.h3`
  margin-top: 5%;
  text-align: center;
`;

export const Styles = {
  inputLabel: {
    color: colors.darkOrange,
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "bold",
    alignSelf: "center",
    marginRight: 10,
  },
  input: {
    backgroundColor: colors.darkOrange,
    width: 100,
    fontSize: 17.5,
    fontWeight: "bold",
    height: 40,
    borderRadius: 10,
    padding: 20,
    marginTop: 10,
    marginBottom: 10,
    color: colors.light,
    boxShadow: "0 0 0.5rem rgba(215, 116, 61, 0.69)",
  },
  button: {
    color: colors.light,
    width: 266,
    height: 61,
    borderRadius: 15,
    backgroundColor: colors.darkOrange,
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    letterSpacing: 3,
    marginBottom: 5,
    boxShadow: "0 0 0.5rem rgba(215, 116, 61, 0.69)",
    transition: "all 0.3s",

    "&:hover": {
      backgroundColor: "#f0f0f0",
      color: "rgba(215, 116, 61, 0.69)",
      boxShadow: "0 0 1rem rgba(215, 116, 61, 0.69)",
      fontSize: 22,
    },
  },
  alert: {
    marginTop: 40,
    fontWeight: "bold",
    fontFamily: "Roboto",
    fontSize: 18,
  },
  requireSpan: {
    color: "red",
    fontWeight: "bold",
    fontSize: 12,
    alignSelf: "center",
  },
};
