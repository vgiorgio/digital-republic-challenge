import React, { useEffect, useState } from "react";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { colors } from "../../assets/theme/theme";
import { Button, InputLabel, Input, Alert } from "@mui/material";
import {
  SectionTitle,
  MainSection,
  MainForm,
  Styles,
  InputSection,
  InputContainer,
  TextSectionDivider,
  InputArea,
  SectionSubtitle,
} from "./styles";
import api from "../../services/api";

type FormData = [
  {
    width: number;
    height: number;
    windowsNum: number;
    doorsNum: number;
  },
  {
    width: number;
    height: number;
    windowsNum: number;
    doorsNum: number;
  },
  {
    width: number;
    height: number;
    windowsNum: number;
    doorsNum: number;
  },
  {
    width: number;
    height: number;
    windowsNum: number;
    doorsNum: number;
  }
];

function Form({ query }: any) {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>();
  const [alert, setAlert] = useState({ status: false, message: "" });

  const [cans, setCans] = useState<any>({});

  /* called upon the form submit - api requests */
  const onSubmit: SubmitHandler<FormData> = async (data) => {
    setAlert({ status: false, message: "" });

    await api
      .post("/cans", { queries: [data[0], data[1], data[2], data[3]] })
      .then((res: any) => {
        // console.log(res.data);
        setCans(res.data);
      })
      .catch((err: any) => {
        // console.log(err.response.data);
        setCans({});
        setAlert({ status: true, message: err.response.data });
      });
  };

  useEffect(() => {
    // console.log(cans);
    query(cans);
  }, [cans]);

  return (
    <MainSection style={{ color: colors.light }}>
      <SectionTitle style={{ color: colors.darkOrange }}>
        Insira os dados
      </SectionTitle>
      <SectionSubtitle style={{ color: colors.lightOrange }}>
        Medidas de uma janela: 2,00 x 1,20 metros
        <br />
        Medidas de uma porta: 0,80 x 1,90 metros
      </SectionSubtitle>
      <MainForm onSubmit={handleSubmit(onSubmit)}>
        <InputContainer>
          {/* First Wall data */}
          <InputSection>
            <TextSectionDivider style={{ color: colors.darkOrange }}>
              Primeira parede
            </TextSectionDivider>
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Largura: </InputLabel>
              <Controller
                name="0.width"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[0]?.width && (
              <span style={Styles.requireSpan}>{errors[0]?.width.message}</span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Altura: </InputLabel>
              <Controller
                name="0.height"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[0]?.height && (
              <span style={Styles.requireSpan}>
                {errors[0]?.height.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de janelas: </InputLabel>
              <Controller
                name="0.windowsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[0]?.windowsNum && (
              <span style={Styles.requireSpan}>
                {errors[0]?.windowsNum.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de portas: </InputLabel>
              <Controller
                name="0.doorsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[0]?.doorsNum && (
              <span style={Styles.requireSpan}>
                {errors[0]?.doorsNum.message}
              </span>
            )}
          </InputSection>

          {/* Second Wall data */}
          <InputSection>
            <TextSectionDivider style={{ color: colors.darkOrange }}>
              Segunda parede
            </TextSectionDivider>
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Largura: </InputLabel>
              <Controller
                name="1.width"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[1]?.width && (
              <span style={Styles.requireSpan}>{errors[1]?.width.message}</span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Altura: </InputLabel>
              <Controller
                name="1.height"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[1]?.height && (
              <span style={Styles.requireSpan}>
                {errors[1]?.height.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de janelas: </InputLabel>
              <Controller
                name="1.windowsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[1]?.windowsNum && (
              <span style={Styles.requireSpan}>
                {errors[1]?.windowsNum.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de portas: </InputLabel>
              <Controller
                name="1.doorsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[1]?.doorsNum && (
              <span style={Styles.requireSpan}>
                {errors[1]?.doorsNum.message}
              </span>
            )}
          </InputSection>

          {/* Third Wall data */}
          <InputSection>
            <TextSectionDivider style={{ color: colors.darkOrange }}>
              Terceira parede
            </TextSectionDivider>
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Largura: </InputLabel>
              <Controller
                name="2.width"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[2]?.width && (
              <span style={Styles.requireSpan}>{errors[2]?.width.message}</span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Altura: </InputLabel>
              <Controller
                name="2.height"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[2]?.height && (
              <span style={Styles.requireSpan}>
                {errors[2]?.height.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de janelas: </InputLabel>
              <Controller
                name="2.windowsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[2]?.windowsNum && (
              <span style={Styles.requireSpan}>
                {errors[2]?.windowsNum.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de portas: </InputLabel>
              <Controller
                name="2.doorsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[2]?.doorsNum && (
              <span style={Styles.requireSpan}>
                {errors[2]?.doorsNum.message}
              </span>
            )}
          </InputSection>

          {/* Fourth Wall data */}
          <InputSection>
            <TextSectionDivider style={{ color: colors.darkOrange }}>
              Quarta parede
            </TextSectionDivider>
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Largura: </InputLabel>
              <Controller
                name="3.width"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[3]?.width && (
              <span style={Styles.requireSpan}>{errors[3]?.width.message}</span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>Altura: </InputLabel>
              <Controller
                name="3.height"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={1.5}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0, step: 0.1 }}
                  />
                )}
              />
            </InputArea>
            {errors[3]?.height && (
              <span style={Styles.requireSpan}>
                {errors[3]?.height.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de janelas: </InputLabel>
              <Controller
                name="3.windowsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[3]?.windowsNum && (
              <span style={Styles.requireSpan}>
                {errors[3]?.windowsNum.message}
              </span>
            )}
            <InputArea>
              <InputLabel style={Styles.inputLabel}>N° de portas: </InputLabel>
              <Controller
                name="3.doorsNum"
                control={control}
                rules={{
                  required: "Campo obrigatório",
                }}
                defaultValue={0}
                render={({ field }) => (
                  <Input
                    {...field}
                    type="number"
                    size="medium"
                    style={Styles.input}
                    disableUnderline={true}
                    inputProps={{ min: 0 }}
                  />
                )}
              />
            </InputArea>
            {errors[3]?.doorsNum && (
              <span style={Styles.requireSpan}>
                {errors[3]?.doorsNum.message}
              </span>
            )}
          </InputSection>
        </InputContainer>
        {alert.status && (
          <Alert style={Styles.alert} severity="error">
            {alert.message}
          </Alert>
        )}
        <Button sx={Styles.button} type="submit">
          Buscar
        </Button>
      </MainForm>
    </MainSection>
  );
}

export default Form;
