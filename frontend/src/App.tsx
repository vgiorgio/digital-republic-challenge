import React from "react";
import { GlobalStyle } from "./pages/globalStyle/global";
import Home from "./pages/Home";

function App() {
  return (
    <>
      <GlobalStyle />
      <Home />
    </>
  );
}

export default App;
