import React, { useEffect, useState } from "react";
import { colors } from "../../assets/theme/theme";
import Footer from "../../components/Footer";
import Form from "../../components/Form";
import Navbar from "../../components/Navbar";
import QueryResult from "../../components/QueryResult";
import TextSection from "../../components/TextSection";
import { MainContainer } from "./styles";

function Home() {
  const [query, setQuery] = useState({});

  const handleQuery = (data: any) => {
    setQuery(data);
  };

  useEffect(() => {
    // console.log(query);
  }, [query]);

  return (
    <MainContainer style={{ backgroundColor: colors.light }}>
      <Navbar />
      <TextSection />
      <Form query={handleQuery} />
      <QueryResult cans={query} />
      <Footer />
    </MainContainer>
  );
}

export default Home;
