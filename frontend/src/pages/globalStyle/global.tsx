import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`

*{
    margin: 0;
    padding: 0;
    outline:0;
    box-sizing:border-box;
    
}

#root{
    margin:0 auto;
}

body{
    max-width: 100vw;
    font-family: 'Roboto', sans-serif;
}

/* SCROLLBAR */
::-webkit-scrollbar {
  background-color: rgba(215, 116, 61, 0.69);
  width: 10px;
}

::-webkit-scrollbar-thumb {
    width: 10px;
    background-color: #F0F0F0;
    border-radius: 10px;
}

`;
